--[[
Lua script for Blaster Master. It shold work for either of the three releases.
This horrible coding nightmare is brought to you by Reimu.

Features:
- Map on the subscreen by pressing Select. (The maps are are hand-drawn, so this won't work on romhacks)
    Displays rooms, area entrances, dungeon entrances, and the tank's location.
- Toggling of collected upgrades, similarly to Super Metroid.
- Using the Gun energy to refill Hover fuel, pressing Down+B.
- Meters display energy more accurately and have a slightly different design. Secondary meters are added to display all stats.

Known bugs:
- Checking the map while scrolling in a door is glitchy sometimes (then again the game itself glitches up when pausing at that time, too)
- Area 6's map is a mess, one room marks two rooms at once
]]

-- General functions
function rampals(RAMOff) -- get in-game colors, needs to be done way better
if (memory.readbyte(RAMOff+0x00) < 0x80) then PT0 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x00)) else PT0 = "clear" end
if (memory.readbyte(RAMOff+0x01) < 0x80) then PT1 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x01)) else PT1 = "clear" end
if (memory.readbyte(RAMOff+0x02) < 0x80) then PT2 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x02)) else PT2 = "clear" end
if (memory.readbyte(RAMOff+0x03) < 0x80) then PT3 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x03)) else PT3 = "clear" end
if (memory.readbyte(RAMOff+0x04) < 0x80) then PT4 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x04)) else PT4 = "clear" end
if (memory.readbyte(RAMOff+0x05) < 0x80) then PT5 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x05)) else PT5 = "clear" end
if (memory.readbyte(RAMOff+0x06) < 0x80) then PT6 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x06)) else PT6 = "clear" end
if (memory.readbyte(RAMOff+0x07) < 0x80) then PT7 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x07)) else PT7 = "clear" end
if (memory.readbyte(RAMOff+0x08) < 0x80) then PT8 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x08)) else PT8 = "clear" end
if (memory.readbyte(RAMOff+0x09) < 0x80) then PT9 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x09)) else PT9 = "clear" end
if (memory.readbyte(RAMOff+0x0A) < 0x80) then PTA = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0A)) else PTA = "clear" end
if (memory.readbyte(RAMOff+0x0B) < 0x80) then PTB = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0B)) else PTB = "clear" end
if (memory.readbyte(RAMOff+0x0C) < 0x80) then PTC = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0C)) else PTC = "clear" end
if (memory.readbyte(RAMOff+0x0D) < 0x80) then PTD = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0D)) else PTD = "clear" end
if (memory.readbyte(RAMOff+0x0E) < 0x80) then PTE = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0E)) else PTE = "clear" end
if (memory.readbyte(RAMOff+0x0F) < 0x80) then PTF = "P"..string.format("%02X",memory.readbyte(RAMOff+0x0F)) else PTF = "clear" end
if (memory.readbyte(RAMOff+0x10) < 0x80) then PS0 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x10)) else PS0 = "clear" end
if (memory.readbyte(RAMOff+0x11) < 0x80) then PS1 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x11)) else PS1 = "clear" end
if (memory.readbyte(RAMOff+0x12) < 0x80) then PS2 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x12)) else PS2 = "clear" end
if (memory.readbyte(RAMOff+0x13) < 0x80) then PS3 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x13)) else PS3 = "clear" end
if (memory.readbyte(RAMOff+0x14) < 0x80) then PS4 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x14)) else PS4 = "clear" end
if (memory.readbyte(RAMOff+0x15) < 0x80) then PS5 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x15)) else PS5 = "clear" end
if (memory.readbyte(RAMOff+0x16) < 0x80) then PS6 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x16)) else PS6 = "clear" end
if (memory.readbyte(RAMOff+0x17) < 0x80) then PS7 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x17)) else PS7 = "clear" end
if (memory.readbyte(RAMOff+0x18) < 0x80) then PS8 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x18)) else PS8 = "clear" end
if (memory.readbyte(RAMOff+0x19) < 0x80) then PS9 = "P"..string.format("%02X",memory.readbyte(RAMOff+0x19)) else PS9 = "clear" end
if (memory.readbyte(RAMOff+0x1A) < 0x80) then PSA = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1A)) else PSA = "clear" end
if (memory.readbyte(RAMOff+0x1B) < 0x80) then PSB = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1B)) else PSB = "clear" end
if (memory.readbyte(RAMOff+0x1C) < 0x80) then PSC = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1C)) else PSC = "clear" end
if (memory.readbyte(RAMOff+0x1D) < 0x80) then PSD = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1D)) else PSD = "clear" end
if (memory.readbyte(RAMOff+0x1E) < 0x80) then PSE = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1E)) else PSE = "clear" end
if (memory.readbyte(RAMOff+0x1F) < 0x80) then PSF = "P"..string.format("%02X",memory.readbyte(RAMOff+0x1F)) else PSF = "clear" end
end
function DrawLineP(offset, x, y, color1, color2, color3, color0, hflip) --draws from an offset, to be used by DrawTileP
 if (hflip ~= true) then hflip = false end
 if (color0 == nil) then color0 = "clear" end
 if (color1 == nil) then color1 = "P00" end
 if (color2 == nil) then color2 = "P10" end
 if (color3 == nil) then color3 = "P20" end
 c = {}
 c[0] = color0
 c[1] = color1
 c[2] = color2
 c[3] = color3
 
-- vvvv there's probably a better way to do this part
if (hflip == false) then
 LBa = rom.readbyte(offset)
  LAa = {}
  LAa[0] = bit.rshift(LBa, 7)%2
  LAa[1] = bit.rshift(LBa, 6)%2
  LAa[2] = bit.rshift(LBa, 5)%2
  LAa[3] = bit.rshift(LBa, 4)%2
  LAa[4] = bit.rshift(LBa, 3)%2
  LAa[5] = bit.rshift(LBa, 2)%2
  LAa[6] = bit.rshift(LBa, 1)%2
  LAa[7] = LBa%2
 LBb = rom.readbyte(offset+8)
  LAb = {}
  LAb[0] = (bit.rshift(LBb, 7)%2)*2
  LAb[1] = (bit.rshift(LBb, 6)%2)*2
  LAb[2] = (bit.rshift(LBb, 5)%2)*2
  LAb[3] = (bit.rshift(LBb, 4)%2)*2
  LAb[4] = (bit.rshift(LBb, 3)%2)*2
  LAb[5] = (bit.rshift(LBb, 2)%2)*2
  LAb[6] = (bit.rshift(LBb, 1)%2)*2
  LAb[7] = (LBb%2)*2
end
if (hflip == true) then
 LBa = rom.readbyte(offset)
  LAa = {}
  LAa[7] = bit.rshift(LBa, 7)%2
  LAa[6] = bit.rshift(LBa, 6)%2
  LAa[5] = bit.rshift(LBa, 5)%2
  LAa[4] = bit.rshift(LBa, 4)%2
  LAa[3] = bit.rshift(LBa, 3)%2
  LAa[2] = bit.rshift(LBa, 2)%2
  LAa[1] = bit.rshift(LBa, 1)%2
  LAa[0] = LBa%2
 LBb = rom.readbyte(offset+8)
  LAb = {}
  LAb[7] = (bit.rshift(LBb, 7)%2)*2
  LAb[6] = (bit.rshift(LBb, 6)%2)*2
  LAb[5] = (bit.rshift(LBb, 5)%2)*2
  LAb[4] = (bit.rshift(LBb, 4)%2)*2
  LAb[3] = (bit.rshift(LBb, 3)%2)*2
  LAb[2] = (bit.rshift(LBb, 2)%2)*2
  LAb[1] = (bit.rshift(LBb, 1)%2)*2
  LAb[0] = (LBb%2)*2
end

 LDT = {}
  for t=0,7 do
  LDT[t] = LAa[t] + LAb[t]
  end

 for n=0,7 do
  gui.pixel(x+n,y,c[LDT[n]])
 end
 
end
function DrawTileP(offset, x, y, color1, color2, color3, color0, vflip, hflip) -- draws a tile from ROM
 if (vflip ~= true) then vflip = false end
 if (vflip == true) then
 L = 7
 for L=7,0,-1 do
  DrawLineP(offset+L, x, y-L+7, color1, color2, color3, color0, hflip)
 end
 end
 if (vflip == false) then
 for L=0,7 do
  DrawLineP(offset+L, x, y+L, color1, color2, color3, color0, hflip)
 end
 end
end
GameFont = {}
function FontOff(NumOff,UpLetOff,LoLetOff) --sets offsets for font
 if (NumOff ~= nil) then
  for i=48, 57 do GameFont[i] = NumOff + 0x10 * (i-48) end
 end-- 0-9
 if (UpLetOff ~= nil) then
  for i=65, 90 do GameFont[i] = UpLetOff + 0x10 * (i-65) end
 end -- A-Z
 if (LoLetOff ~= nil) then
  for i=97,122 do GameFont[i] = LoLetOff + 0x10 * (i-97) end
 end -- a-z
end
function DrawTextP(str,x,y,color1,color2,color3,color0) --really slow, starts lagging with about 0x40 tiles
 str = tostring(str)
 ox = x
 for i=1,#str do
  b = string.byte(str,i)
   if (b == 10) then 
    y = y+8
	x = ox
   else
    a = GameFont[b]
	 if (a ~= nil) then DrawTileP(a,x,y,color1,color2,color3,color0,false,false) end
	x = x+8
   end
  end
 end
FontOff(0x27310,0x27420)
  GameFont[string.byte("!")] = 0x027220
  GameFont[string.byte("(")] = 0x027290
  GameFont[string.byte(")")] = 0x0272A0
  GameFont[string.byte(".")] = 0x0273C0

-- Actual game functions
function DrawTube(x,y,d,l) 
if (l == nil) then l = 1 end
l = l-1
if (d == 0) then -- horizontal = 0
 gui.line(x*8,y*8+1,(x+l)*8-1,y*8+1,PTF) gui.line(x*8,y*8+2,(x+l)*8-1,y*8+2,PTE) gui.line(x*8,y*8+3,(x+l)*8-1,y*8+3,PTD)
 gui.line(x*8,y*8+4,(x+l)*8-1,y*8+4,PTD) gui.line(x*8,y*8+5,(x+l)*8-1,y*8+5,PTE) gui.line(x*8,y*8+6,(x+l)*8-1,y*8+6,PTF)
end
if (d == 1) then -- vertical = 1
 gui.line(x*8+1,y*8,x*8+1,(y+l)*8-1,PTF) gui.line(x*8+2,y*8,x*8+2,(y+l)*8-1,PTE) gui.line(x*8+3,y*8,x*8+3,(y+l)*8-1,PTD)
 gui.line(x*8+4,y*8,x*8+4,(y+l)*8-1,PTD) gui.line(x*8+5,y*8,x*8+5,(y+l)*8-1,PTE) gui.line(x*8+6,y*8,x*8+6,(y+l)*8-1,PTF)
end
 
end
function DrawArrow(x,y,d,c) -- 0v 1^ 2L 37 4>
if (d == nil) then d = 4 end
 if (d == 0) then
  gui.line(x+3,y,x+3,y+7,c) gui.line(x+4,y,x+4,y+7,c)
  gui.line(x,y+4,x+7,y+4,c) gui.line(x+1,y+5,x+6,y+5,c) gui.line(x+2,y+6,x+5,y+6,c)
 end
 if (d == 1) then
  gui.line(x+3,y,x+3,y+7,c) gui.line(x+4,y,x+4,y+7,c)
  gui.line(x,y+3,x+7,y+3,c) gui.line(x+1,y+2,x+6,y+2,c) gui.line(x+2,y+1,x+5,y+1,c)
 end

end

function havepowerups()
powerups = memory.readbyte(0x0099) -- mostly inventory screen
h_hover = (powerups % 2 == 1) -- enables wheels, but doesn't enable bar
h_dive = (bit.rshift(powerups, 1) % 2 == 1) 
h_wall1 = (bit.rshift(powerups, 2) % 2 == 1) 
h_wall2 = (bit.rshift(powerups, 3) % 2 == 1) -- needs wall 1 to work
h_crusher = (bit.rshift(powerups, 4) % 2 == 1) -- only tells if you "have" it, doesn't enable crusher shots
h_spike = (bit.rshift(powerups, 5) % 2 == 1) -- unused
h_hyper = (bit.rshift(powerups, 6) % 2 == 1) -- same as crusher
h_key = (bit.rshift(powerups, 7) % 2 == 1) -- useless

abilities = memory.readbyte(0x03FC) -- secondary flags to actually use some of the above, follow boss order
hyper_on = (abilities % 2 == 1) -- enables hyper
crusher_on = (bit.rshift(abilities, 1) % 2 == 1) -- the same regardless of if hyper is active or not
hover_bar = (bit.rshift(abilities, 2) % 2 == 1) -- allows to collect fuel and actually hover
use_key = (bit.rshift(abilities, 3) % 2 == 1) -- useable
ab_dive = (bit.rshift(abilities, 4) % 2 == 1) -- Dive (?)
ab_wall1 = (bit.rshift(abilities, 5) % 2 == 1) -- Wall1 (?)
ab_wall2 = (bit.rshift(abilities, 6) % 2 == 1) -- Wall2 (?)
ab_spike = (bit.rshift(abilities, 7) % 2 == 1) -- unused

end
EquipCursor = 0
ShowMapM = false

function PlaySFX(a)
memory.writebyte(0x0377,a)
end

function DrawCurrMap()
 DrawTextP("AREA "..CurrAreaN+1,13*8,5*8,PT9,PT8,PT8,PT8)
 if (InTank) then
 MPosX = PlayerMX
 MPosY = PlayerMY
 MPosT = CurrAreaN
 else
 MPosX = SofiaMX
 MPosY = SofiaMY
 MPosT = SofiaMap-8
 end
 --gui.rect(8*8,6*8,(8+16)*8,(6+12)*8,"clear",PTB)
 if (CurrAreaN == 0) then
  gui.rect(8*8,8*8-4,9*8,10*8,"clear",PTA)
  gui.rect(9*8,7*8,16*8,11*8,"clear",PTA)
  gui.rect(16*8,10*8,17*8,12*8,"clear",PTA)
  gui.line(17*8,14*8-3,24*8,14*8-3,PTB) gui.rect(17*8,11*8,24*8,15*8,"clear",PTA)
  gui.rect(16*8,13*8,17*8,15*8,"clear",PTA)
  --
  gui.rect(8*8+2,9*8+4,8*8+6,9*8+7,"clear",PT9)
  gui.rect(16*8+2,14*8+4,16*8+6,14*8+7,"clear",PT9)
  gui.pixel(17*8+4,12*8+4,PT9) gui.pixel(18*8+4,12*8+4,PT9) gui.pixel(19*8+4,12*8+4,PT9)
  gui.pixel(17*8+4,14*8+4,PT9) gui.pixel(23*8+4,13*8+4,PT9)
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=0) and (MPosY<48)) then gui.rect(8*8,8*8-4,9*8,10*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<128)) and ((MPosY>=0) and (MPosY<64)) then gui.rect(9*8,7*8,16*8,11*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=48) and (MPosY<80)) then gui.rect(16*8,10*8,17*8,12*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<128)) and ((MPosY>=64) and (MPosY<128)) then gui.rect(17*8,11*8,24*8,15*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=96) and (MPosY<128)) then gui.rect(16*8,13*8,17*8,15*8,"clear",PT9) end
  end
 end
 if (CurrAreaN == 1) then 
  gui.rect(9*8,17*8,13*8,18*8,"clear",PTA)
  gui.rect(13*8,13*8,15*8,18*8,"clear",PTA)
  gui.rect(15*8,13*8,17*8,14*8,"clear",PTA)
  gui.rect(17*8,10*8,19*8,14*8,"clear",PTA)
  gui.rect(19*8,13*8,21*8,14*8,"clear",PTA)
  gui.rect(15*8,6*8,17*8,13*8,"clear",PTA) --main tall room
  gui.rect(12*8,12*8,15*8,13*8,"clear",PTA) --branch to 7
  gui.rect(11*8,10*8,12*8,13*8,"clear",PTA)
  gui.rect(12*8,10*8,14*8,12*8,"clear",PTA)
  gui.rect(14*8,11*8,15*8,12*8,"clear",PTA)
  gui.rect(14*8,10*8,15*8,11*8,"clear",PTA) -- to 3
  gui.rect(17*8,8*8,19*8,9*8,"clear",PTA)
  gui.rect(17*8,6*8,19*8,7*8,"clear",PTA)
  gui.rect(19*8,6*8,21*8,8*8,"clear",PTA)
  gui.rect(17*8,7*8,19*8,8*8,"clear",PTA)
  --
  gui.rect(9*8+2,17*8+4,9*8+6,17*8+7,"clear",PT9)
  gui.rect(14*8+2,11*8+4,14*8+6,11*8+7,"clear",PT9)
  gui.rect(14*8+2,10*8+4,14*8+6,10*8+7,"clear",PT9)
  gui.pixel(20*8+4,13*8+4,PT9) gui.pixel(18*8+4,8*8+4,PT9)
  gui.pixel(15*8+4,6*8+4,PT9) gui.pixel(17*8+4,7*8+4,PT9)
  DrawArrow(14*8,9*8,0,PTB) DrawArrow(15*8,17*8,1,PTB)
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=64) and (MPosX<128)) and ((MPosY>=64) and (MPosY<80)) then gui.rect(9*8,17*8,13*8,18*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<32)) and ((MPosY>=0) and (MPosY<80)) then gui.rect(13*8,13*8,15*8,18*8,"clear",PT9) end
   if ((MPosX>=32) and (MPosX<64)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(15*8,13*8,17*8,14*8,"clear",PT9) end
   if ((MPosX>=64) and (MPosX<96)) and ((MPosY>=80) or (MPosY<16)) then gui.rect(17*8,10*8,19*8,14*8,"clear",PT9) end
   if ((MPosX>=96) and (MPosX<128)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(19*8,13*8,21*8,14*8,"clear",PT9) end
   if ((MPosX>=32) and (MPosX<64)) and ((MPosY>=16) and (MPosY<128)) then gui.rect(15*8,6*8,17*8,13*8,"clear",PT9) end --
   if ((MPosX>=112) or (MPosX<32)) and ((MPosY>=112) and (MPosY<128)) then gui.rect(12*8,12*8,15*8,13*8,"clear",PT9) end --
   if ((MPosX>=96) and (MPosX<112)) and ((MPosY>=80) and (MPosY<128)) then gui.rect(11*8,10*8,12*8,13*8,"clear",PT9) end
   if ((MPosX>=112) or (MPosX<16)) and ((MPosY>=80) and (MPosY<112)) then gui.rect(12*8,10*8,14*8,12*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=96) and (MPosY<112)) then gui.rect(14*8,11*8,15*8,12*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(14*8,10*8,15*8,11*8,"clear",PT9) end --
   if ((MPosX>=64) and (MPosX<96)) and ((MPosY>=48) and (MPosY<64)) then gui.rect(17*8,8*8,19*8,9*8,"clear",PT9) end
   if ((MPosX>=64) and (MPosX<96)) and ((MPosY>=16) and (MPosY<32)) then gui.rect(17*8,6*8,19*8,7*8,"clear",PT9) end
   if ((MPosX>=96) and (MPosX<128)) and ((MPosY>=16) and (MPosY<48)) then gui.rect(19*8,6*8,21*8,8*8,"clear",PT9) end
   if ((MPosX>=64) and (MPosX<96)) and ((MPosY>=32) and (MPosY<48)) then gui.rect(17*8,7*8,19*8,8*8,"clear",PT9) end
  end
 end
 if (CurrAreaN == 2) then 
  gui.rect(16*8,6*8,19*8,7*8,"clear",PTA)
  gui.rect(19*8,6*8,20*8,8*8,"clear",PTA)
  gui.line(19*8,7*8,14*8,7*8,PTA) -- large weirdly shaped room
   gui.line(14*8,7*8,14*8,8*8,PTA) gui.line(14*8,8*8,16*8,8*8,PTA) gui.line(16*8,8*8,15*8-4,9*8+4,PTA)
   gui.line(15*8-4,9*8+4,13*8,9*8+4,PTA) gui.line(13*8,9*8+4,13*8,11*8,PTA) gui.line(13*8,11*8,14*8,11*8,PTA)
   gui.line(14*8,11*8,14*8,12*8,PTA) gui.line(14*8,12*8,19*8,12*8,PTA) gui.line(19*8,12*8,19*8,7*8,PTA)
  gui.rect(19*8,8*8,21*8,9*8,"clear",PTA) --warp point
  gui.line(13*8,15*8,14*8+2,15*8,PTA) gui.line(13*8,18*8,13*8,15*8,PTA) -- smaller weird room
   gui.line(14*8+2,15*8,14*8+2,16*8+2,PTA) gui.line(14*8+2,16*8+2,14*8+6,16*8+2,PTA) gui.line(14*8+6,16*8+2,14*8+6,17*8+2,PTA)
   gui.line(14*8+6,17*8+2,14*8,17*8+2,PTA) gui.line(14*8,17*8+2,14*8,18*8,PTA) gui.line(14*8,18*8,13*8,18*8,PTA)
  gui.rect(12*8,14*8,13*8,16*8,"clear",PTA) -- 1x2 
  gui.rect(13*8,13*8,15*8,15*8,"clear",PTA) -- 2x2 
  gui.rect(15*8,13*8,19*8,14*8,"clear",PTA)
  gui.rect(19*8,12*8,20*8,14*8,"clear",PTA)
  gui.rect(13*8,11*8,14*8,13*8,"clear",PTA) gui.rect(13*8,12*8,19*8,13*8,"clear",PTA) -- 3-way room
   gui.rect(13*8+1,12*8,14*8,13*8-1,"clear",PT8) -- 
  gui.rect(12*8,12*8,13*8,14*8,"clear",PTA)
  gui.rect(12*8,10*8,13*8,12*8,"clear",PTA) -- 
  gui.rect(19*8,9*8,20*8,11*8,"clear",PTA)
  gui.rect(19*8,11*8,20*8,12*8,"clear",PTA)
  --
  gui.rect(16*8+2,6*8+4,16*8+6,6*8+7,"clear",PT9)
  gui.rect(14*8+2,7*8+4,14*8+6,7*8+7,"clear",PT9)
  gui.pixel(14*8+4,17*8,PT9)
  gui.pixel(12*8+4,13*8+4,PT9) gui.pixel(13*8+4,13*8+4,PT9)
  gui.pixel(13*8+2,10*8-2,PT9)
  gui.pixel(19*8+4,11*8+4,PT9)
  DrawArrow(21*8,8*8,0,PTB) DrawArrow(12*8,16*8+2,1,PTB)
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then -- 
   if ((MPosX>=80) and (MPosX<128)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(16*8,6*8,19*8,7*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(19*8,6*8,20*8,8*8,"clear",PT9) end
   -- No
    if ((((MPosX>=32) and (MPosX<128)) and ((MPosY>=16) and (MPosY<96)))
    	and not(((((MPosX>=32) and (MPosX<52)) and ((MPosY>=16) and (MPosY<58)))
	                 or (((MPosX>=32) and (MPosX<64)) and ((MPosY>=32) and (MPosY<52))))
				or((((MPosX>=32) and (MPosX<128)) and ((MPosY>=96) and (MPosY<112)))
    	             or (((MPosX>=32) and (MPosX<52)) and ((MPosY>=80) and (MPosY<112)))))
		) then
	   gui.line(19*8,7*8,14*8,7*8,PT9) -- No
       gui.line(14*8,7*8,14*8,8*8,PT9) gui.line(14*8,8*8,16*8,8*8,PT9) gui.line(16*8,8*8,15*8-4,9*8+4,PT9)
       gui.line(15*8-4,9*8+4,13*8,9*8+4,PT9) gui.line(13*8,9*8+4,13*8,11*8,PT9) gui.line(13*8,11*8,14*8,11*8,PT9)
       gui.line(14*8,11*8,14*8,12*8,PT9) gui.line(14*8,12*8,19*8,12*8,PT9) gui.line(19*8,12*8,19*8,7*8,PT9)
	end
   if ((MPosX>=0) and (MPosX<32)) and ((MPosY>=32) and (MPosY<48)) then gui.rect(19*8,8*8,21*8,9*8,"clear",PT9) end
   -- small
    if ((((MPosX>=32) and (MPosX<52)) and ((MPosY>=16) and (MPosY<58)))
	   or (((MPosX>=32) and (MPosX<64)) and ((MPosY>=32) and (MPosY<52)))) then
	   gui.line(13*8,15*8,14*8+2,15*8,PT9) gui.line(13*8,18*8,13*8,15*8,PT9) -- 
       gui.line(14*8+2,15*8,14*8+2,16*8+2,PT9) gui.line(14*8+2,16*8+2,14*8+6,16*8+2,PT9) gui.line(14*8+6,16*8+2,14*8+6,17*8+2,PT9)
       gui.line(14*8+6,17*8+2,14*8,17*8+2,PT9) gui.line(14*8,17*8+2,14*8,18*8,PT9) gui.line(14*8,18*8,13*8,18*8,PT9)
	end
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(12*8,14*8,13*8,16*8,"clear",PT9) end --
   if ((MPosX>=32) and (MPosX<64)) and ((MPosY>=112) or (MPosY<16)) then gui.rect(13*8,13*8,15*8,15*8,"clear",PT9) end --
   if ((MPosX>=64) and (MPosX<128)) and ((MPosY>=112) and (MPosY<128)) then gui.rect(15*8,13*8,19*8,14*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=96) and (MPosY<128)) then gui.rect(19*8,12*8,20*8,14*8,"clear",PT9) end
   -- 3-way room
    if (((MPosX>=32) and (MPosX<128)) and ((MPosY>=96) and (MPosY<112)))
    	or (((MPosX>=32) and (MPosX<52)) and ((MPosY>=80) and (MPosY<112))) then
		gui.rect(13*8,11*8,14*8,13*8,"clear",PT9) gui.rect(13*8,12*8,19*8,13*8,"clear",PT9)
        gui.rect(13*8+1,12*8,14*8,13*8-1,"clear",PT8)
    end
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=96) and (MPosY<128)) then gui.rect(12*8,12*8,13*8,14*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=64) and (MPosY<96)) then gui.rect(12*8,10*8,13*8,12*8,"clear",PT9) end --
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=48) and (MPosY<80)) then gui.rect(19*8,9*8,20*8,11*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(19*8,11*8,20*8,12*8,"clear",PT9) end   
  end
 end
 if (CurrAreaN == 3) then 
  gui.rect(12*8,8*8,16*8,9*8,"clear",PTA)
  gui.rect(16*8,8*8,18*8,10*8,"clear",PTA)
  gui.rect(18*8,8*8,20*8,10*8,"clear",PTA) --lock
  gui.rect(13*8,9*8,16*8,10*8,"clear",PTA)
  gui.rect(12*8,9*8,13*8,11*8,"clear",PTA)
  gui.rect(13*8,10*8,20*8,16*8,"clear",PTA) --maze
  --
  gui.rect(12*8+2,8*8+4,12*8+6,8*8+7,"clear",PT9)
  gui.rect(19*8+2,9*8+4,19*8+6,9*8+7,"clear",PT9)
  gui.pixel(12*8+4,9*8+4,PT9)
  gui.pixel(19*8+4,10*8+4,PT9) gui.pixel(19*8+4,14*8+4,PT9)
  gui.pixel(13*8+4,14*8+4,PT9)
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=0) and (MPosX<64)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(12*8,8*8,16*8,9*8,"clear",PT9) end
   if ((MPosX>=64) and (MPosX<96)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(16*8,8*8,18*8,10*8,"clear",PT9) end
   if ((MPosX>=96) and (MPosX<128)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(18*8,8*8,20*8,10*8,"clear",PT9) end --
   if ((MPosX>=16) and (MPosX<64)) and ((MPosY>=16) and (MPosY<32)) then gui.rect(13*8,9*8,16*8,10*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=16) and (MPosY<48)) then gui.rect(12*8,9*8,13*8,11*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<128)) and ((MPosY>=32) and (MPosY<128)) then gui.rect(13*8,10*8,20*8,16*8,"clear",PT9) end --
  end
 end
 if (CurrAreaN == 4) then 
   gui.line(15*8,8*8+5,23*8,8*8+5,PTB)
  gui.rect(19*8,8*8,21*8,14*8,"clear",PTA)
  gui.rect(21*8,8*8,23*8,9*8,"clear",PTA) --to 6
   gui.line(21*8,9*8+3,22*8,9*8+3,PTB)
  gui.rect(21*8,9*8,22*8,11*8,"clear",PTA) --
  gui.rect(16*8,13*8,19*8,14*8,"clear",PTA)
  gui.rect(15*8,13*8,16*8,15*8,"clear",PTA)
  gui.rect(13*8,13*8,15*8,14*8,"clear",PTA) --
  gui.rect(12*8,14*8,15*8,16*8,"clear",PTA)
  gui.rect(10*8,14*8,12*8,15*8,"clear",PTA) --
  gui.rect(9*8,15*8,12*8,16*8,"clear",PTA) --boss
  gui.rect(18*8,10*8,19*8,13*8,"clear",PTA) --dive path to surface
  gui.rect(14*8,12*8,18*8,13*8,"clear",PTA)
  gui.rect(13*8,11*8,14*8,13*8,"clear",PTA)
   gui.line(14*8,11*8+2,17*8,11*8+2,PTB)
   gui.line(15*8,10*8+2,18*8,10*8+2,PTB)
  gui.rect(14*8,11*8,17*8,12*8,"clear",PTA) --ruins rooms
  gui.rect(17*8,10*8,18*8,12*8,"clear",PTA) --
  gui.rect(15*8,10*8,17*8,11*8,"clear",PTA) --
   gui.line(14*8,9*8+3,15*8,9*8+3,PTB)
  gui.rect(14*8,9*8,15*8,11*8,"clear",PTA)
  gui.rect(15*8,8*8,19*8,10*8,"clear",PTA)
  --
  gui.rect(19*8+2,8*8+2,19*8+6,8*8+5,"clear",PT9)
  gui.rect(22*8+2,8*8+2,22*8+6,8*8+5,"clear",PT9)
  gui.pixel(21*8+4,10*8+4,PT9) gui.pixel(13*8+4,13*8+4,PT9)
  gui.pixel(10*8+4,14*8+4,PT9) gui.pixel(9*8+4,15*8+4,PT9)
  gui.pixel(18*8+4,8*8+2,PT9)
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=48) and (MPosX<80)) and ((MPosY>=0) and (MPosY<96)) then gui.rect(19*8,8*8,21*8,14*8,"clear",PT9) end
   if ((MPosX>=80) and (MPosX<112)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(21*8,8*8,23*8,9*8,"clear",PT9) end --
   if ((MPosX>=80) and (MPosX<96)) and ((MPosY>=16) and (MPosY<48)) then gui.rect(21*8,9*8,22*8,11*8,"clear",PT9) end --
   if ((MPosX>=0) and (MPosX<48)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(16*8,13*8,19*8,14*8,"clear",PT9) end
   if ((MPosX>=112) and (MPosX<128)) and ((MPosY>=80) and (MPosY<128)) then gui.rect(15*8,13*8,16*8,15*8,"clear",PT9) end
   if ((MPosX>=80) and (MPosX<112)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(13*8,13*8,15*8,14*8,"clear",PT9) end --
   if ((MPosX>=64) and (MPosX<112)) and ((MPosY>=96) and (MPosY<128)) then gui.rect(12*8,14*8,15*8,16*8,"clear",PT9) end
   if ((MPosX>=32) and (MPosX<64)) and ((MPosY>=96) and (MPosY<112)) then gui.rect(10*8,14*8,12*8,15*8,"clear",PT9) end --
   if ((MPosX>=16) and (MPosX<64)) and ((MPosY>=112) and (MPosY<128)) then gui.rect(9*8,15*8,12*8,16*8,"clear",PT9) end -- boss
   if ((MPosX>=32) and (MPosX<48)) and ((MPosY>=32) and (MPosY<80)) then gui.rect(18*8,10*8,19*8,13*8,"clear",PT9) end --
   if ((MPosX>=96) or (MPosX<32)) and ((MPosY>=64) and (MPosY<80)) then gui.rect(14*8,12*8,18*8,13*8,"clear",PT9) end
   if ((MPosX>=80) and (MPosX<96)) and ((MPosY>=48) and (MPosY<80)) then gui.rect(13*8,11*8,14*8,13*8,"clear",PT9) end
   if ((MPosX>=96) or (MPosX<16)) and ((MPosY>=48) and (MPosY<64)) then gui.rect(14*8,11*8,17*8,12*8,"clear",PT9) end --
   if ((MPosX>=16) and (MPosX<32)) and ((MPosY>=32) and (MPosY<64)) then gui.rect(17*8,10*8,18*8,12*8,"clear",PT9) end --
   if ((MPosX>=112) or (MPosX<16)) and ((MPosY>=32) and (MPosY<48)) then gui.rect(15*8,10*8,17*8,11*8,"clear",PT9) end --
   if ((MPosX>=96) and (MPosX<112)) and ((MPosY>=16) and (MPosY<48)) then gui.rect(14*8,9*8,15*8,11*8,"clear",PT9) end
   if ((MPosX>=112) or (MPosX<48)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(15*8,8*8,19*8,10*8,"clear",PT9) end
  end
 end
 if (CurrAreaN == 5) then 
   gui.rect(17*8,13*8,20*8,15*8,"clear",PTB) --large room
   gui.rect(12*8,13*8,15*8,14*8,"clear",PTB) --ice room
   gui.rect(12*8,10*8,20*8,13*8,"clear",PTB) --outside
   gui.rect(15*8,8*8,20*8,9*8,"clear",PTB) --ice tunnel to end
  gui.rect(12*8,15*8,15*8,16*8,"clear",PTA) --entrance
  gui.rect(15*8,14*8,17*8,16*8,"clear",PTA)
  gui.rect(16*8,13*8,17*8,14*8,"clear",PTA) --
  gui.rect(15*8,12*8,16*8,14*8,"clear",PTA) --spiky walls
   gui.rect(16*8,10*8,17*8,13*8,"clear",PTA) --outside wall, not a real room
   gui.rect(15*8,11*8,16*8,12*8,"clear",PTA) --empty room inside tower
  gui.rect(15*8,9*8,16*8,11*8,"clear",PTA)
  gui.rect(13*8,9*8,15*8,10*8,"clear",PTA) --
   gui.rect(15*8,9*8,18*8,10*8,"clear",PTA) --
   gui.rect(18*8,8*8,19*8,10*8,"clear",PTA)
  gui.rect(17*8,7*8,18*8,9*8,"clear",PTA)
  gui.rect(18*8,7*8,20*8,8*8,"clear",PTA) --boss
  --
  gui.rect(12*8+2,15*8+4,12*8+6,15*8+7,"clear",PT9)
  gui.pixel(15*8+4,14*8+4,PT9) gui.pixel(16*8+4,13*8+4,PT9) gui.pixel(13*8+4,9*8+4,PT9)
  gui.pixel(19*8+4,7*8+4,PT9)
  DrawArrow(17*8,15*8,1,PTB) DrawArrow(16*8,7*8,0,PTB)
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=0) and (MPosX<48)) and ((MPosY>=112) and (MPosY<128)) then gui.rect(12*8,15*8,15*8,16*8,"clear",PT9) end --entrance
   if ((MPosX>=48) and (MPosX<80)) and ((MPosY>=96) and (MPosY<128)) then gui.rect(15*8,14*8,17*8,16*8,"clear",PT9) end
   if ((MPosX>=80) and (MPosX<128)) and ((MPosY>=80) and (MPosY<112)) then gui.rect(17*8,13*8,20*8,15*8,"clear",PT9) end --l room
   if ((MPosX>=64) and (MPosX<80)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(16*8,13*8,17*8,14*8,"clear",PT9) end --
   if ((MPosX>=0) and (MPosX<48)) and ((MPosY>=80) and (MPosY<96)) then gui.rect(12*8,13*8,15*8,14*8,"clear",PT9) end --ice room
   if ((MPosX>=48) and (MPosX<64)) and ((MPosY>=64) and (MPosY<96)) then gui.rect(15*8,12*8,16*8,14*8,"clear",PT9) end --spiky walls
   if ((MPosX>=64) and (MPosX<80)) and ((MPosY>=32) and (MPosY<80)) then gui.rect(16*8,10*8,17*8,13*8,"clear",PT9) end --outside wall
   if ((MPosX>=64) or (MPosX<48)) and ((MPosY>=32) and (MPosY<80)) then gui.rect(12*8,10*8,20*8,13*8,"clear",PT9) end --outside
   if ((MPosX>=48) and (MPosX<64)) and ((MPosY>=16) and (MPosY<48)) then gui.rect(15*8,9*8,16*8,11*8,"clear",PT9) end
   if ((MPosX>=16) and (MPosX<48)) and ((MPosY>=16) and (MPosY<32)) then gui.rect(13*8,9*8,15*8,10*8,"clear",PT9) end --
   if ((MPosX>=64) and (MPosX<112)) and ((MPosY>=16) and (MPosY<32)) then gui.rect(15*8,9*8,18*8,10*8,"clear",PT9) end --
   if ((MPosX>=112) and (MPosX<128)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(18*8,8*8,19*8,10*8,"clear",PT9) end
   if ((MPosX>=0) and (MPosX<80)) and ((MPosY>=0) and (MPosY<16)) then gui.rect(15*8,8*8,20*8,9*8,"clear",PT9) end --ice tunnel to end
   if ((MPosX>=80) and (MPosX<96)) and ((MPosY>=112) or (MPosY<16)) then gui.rect(17*8,7*8,18*8,9*8,"clear",PT9) end
   if ((MPosX>=96) and (MPosX<128)) and ((MPosY>=112) and (MPosY<128)) then gui.rect(18*8,7*8,20*8,8*8,"clear",PT9) end --boss
  end
 end
 if (CurrAreaN == 6) then
  DrawTextP("MAP  UNAVAILABLE",8*8,11*8,PTA) -- Area 7 is a maze of disconnected rooms
  gui.line(7*8+4,10*8+4,7*8+17*8+4,10*8+4,PTB) gui.line(7*8+4,12*8+4,7*8+17*8+4,12*8+4,PTB)
 end
 if (CurrAreaN == 7) then 
  if (MPosY <= 64) then -- totally the full map
  gui.rect(15*8,9*8,16*8,11*8,"clear",PTA) --ent.
  gui.rect(12*8,9*8,15*8,12*8,"clear",PTA) --big1
  gui.rect(15*8,11*8,17*8,13*8,"clear",PTA) --2x2
  gui.rect(17*8,9*8,20*8,12*8,"clear",PTA) --big2
  gui.rect(17*8,12*8,20*8,13*8,"clear",PTA)
  gui.rect(14*8,12*8,15*8,13*8,"clear",PTA) --evil
  --
  gui.rect(15*8+2,9*8+4,15*8+6,9*8+7,"clear",PT9)
  gui.pixel(19*8+4,9*8+4,PT9) gui.pixel(19*8+4,12*8+4,PT9)
  elseif (not (((MPosX>=112) or (MPosX<16)) and ((MPosY>=112) and (MPosY<128)))) then
  DrawTextP("8888  8",19*8,5*8,PT9) DrawTextP("88",23*8,5*8,PTA)
  gui.line(8*8,9*8,9*8,8*8,PTA) gui.line(8*8,10*8,10*8,8*8,PTA) gui.line(8*8,11*8,11*8,8*8,PTA) gui.line(8*8,12*8,12*8,8*8,PTA)
  gui.line(8*8,13*8,13*8,8*8,PTA) gui.line(8*8,14*8,14*8,8*8,PTA) gui.line(8*8,15*8,15*8,8*8,PTA) gui.line(8*8,16*8,16*8,8*8,PTA)
  gui.line(9*8,16*8,17*8,8*8,PTA) gui.line(10*8,16*8,18*8,8*8,PTA) gui.line(11*8,16*8,19*8,8*8,PTA) gui.line(12*8,16*8,20*8,8*8,PTA)
  gui.line(13*8,16*8,21*8,8*8,PTA) gui.line(14*8,16*8,22*8,8*8,PTA) gui.line(15*8,16*8,23*8,8*8,PTA)
  gui.line(15*8,16*8,15*8,12*8,PTB) gui.line(15*8+4,16*8,15*8+4,12*8,PTB) gui.line(16*8,16*8,16*8,12*8,PTB) gui.line(16*8+4,16*8,16*8+4,12*8,PTB)
  gui.line(17*8,16*8,17*8,12*8,PTB) gui.line(17*8+4,16*8,17*8+4,12*8,PTB) gui.line(18*8,16*8,18*8,12*8,PTB) gui.line(18*8+4,16*8,18*8+4,12*8,PTB)
  gui.line(19*8,16*8,19*8,12*8,PTB) gui.line(19*8+4,16*8,19*8+4,12*8,PTB) gui.line(20*8,16*8,20*8,12*8,PTB) gui.line(20*8+4,15*8,16*8+4,12*8,PTB)
  gui.line(21*8,16*8,21*8,12*8,PTB) gui.line(21*8+4,16*8,21*8+4,12*8,PTB) gui.line(22*8,16*8,22*8,12*8,PTB) gui.line(22*8+4,15*8,18*8+4,12*8,PTB)
  gui.rect(21*8,10*8,24*8,13*8,PTA) gui.rect(8*8,10*8,22*8,11*8,PT8,PT8) DrawTileP(0x010813,23*8,15*8,PTB,PTA,PTB)
  else
  gui.rect(13*8,5*8,19*8,6*8,PT0,PT0) DrawTextP("TARGET LOCATED",10*8,11*8,PT2)
  end
  --
  if (U_10%4 < 2) and (MPosT == CurrAreaN) then
   if ((MPosX>=48) and (MPosX<64)) and ((MPosY>=0) and (MPosY<32)) then gui.rect(15*8,9*8,16*8,11*8,"clear",PT9) end --ent
   if ((MPosX>=0) and (MPosX<48)) and ((MPosY>=0) and (MPosY<48)) then gui.rect(12*8,9*8,15*8,12*8,"clear",PT9) end --big1
   if ((MPosX>=48) and (MPosX<80)) and ((MPosY>=32) and (MPosY<64)) then gui.rect(15*8,11*8,17*8,13*8,"clear",PT9) end --2x2
   if ((MPosX>=80) and (MPosX<128)) and ((MPosY>=0) and (MPosY<48)) then gui.rect(17*8,9*8,20*8,12*8,"clear",PT9) end --big2
   if ((MPosX>=80) and (MPosX<128)) and ((MPosY>=48) and (MPosY<64)) then gui.rect(17*8,12*8,20*8,13*8,"clear",PT9) end
   if ((MPosX>=32) and (MPosX<48)) and ((MPosY>=48) and (MPosY<64)) then gui.rect(14*8,12*8,15*8,13*8,"clear",PT9) end --evil
   if ((MPosX>=0) and (MPosX<32)) and ((MPosY>=48) and (MPosY<96)) then gui.rect(108,59,113,140,"clear",PT9) end
   if ((MPosX>=32) and (MPosX<112)) and ((MPosY>=64) and (MPosY<96)) then gui.rect(160,52,166,55,"clear",PT9) gui.line(16*8,16*8,16*8,12*8,PT9) end --wall2 is awful
   if ((MPosX>=112) and (MPosX<128)) and ((MPosY>=64) and (MPosY<80)) then DrawTileP(0x00CF50,108,108,PT9,PT9,PT9,PT8) end --
   if ((MPosX>=112) and (MPosX<128)) and ((MPosY>=80) and (MPosY<112)) then gui.rect(22*8,12*8,23*8,13*8,PT9,PT9) gui.line(8*8,14*8,14*8,8*8,PT9) end
   if ((MPosX>=0) and (MPosX<16)) and ((MPosY>=96) and (MPosY<112)) then gui.rect(8*8,9*8,24*8,16*8,PT8) DrawTileP(0x0355D0,17*8,8*8,PT9,PT9,PT9,PTA)
   DrawTileP(0x0355E0,17*8,9*8,PT9,PT9,PT9,PTB) end --grotto
   if ((MPosX>=16) and (MPosX<112)) and ((MPosY>=96) and (MPosY<128)) then DrawTextP("A EA 8 8888",13*8,5*8,PTB) gui.rect(8*8,6*8,12*8,16*8,PT8)
   gui.rect(20*8+2,7*8+4,20*8+6,7*8+7,"clear",PT9) gui.rect(20*8+2,8*8+4,20*8+6,8*8+7,"clear",PT9) gui.rect(20*8+2,9*8+4,20*8+6,9*8+7,"clear",PT9)
   gui.rect(20*8+2,10*8+4,20*8+6,10*8+7,"clear",PT9) gui.rect(20*8+2,11*8+4,20*8+6,11*8+7,"clear",PT9) gui.rect(20*8+2,12*8+4,20*8+6,12*8+7,"clear",PT9) 
   gui.rect(20*8+2,13*8+4,20*8+6,13*8+7,"clear",PT9) gui.rect(20*8+2,14*8+4,20*8+6,14*8+7,"clear",PT9) gui.rect(20*8+2,15*8+4,20*8+6,15*8+7,"clear",PT9)
   gui.rect(20*8+2,16*8+4,20*8+6,16*8+7,"clear",PT9) DrawTextP("8",16*8,6*8,PT9) gui.rect(120,90,140,108,"clear",PT9) end --
   if ((MPosX>=112) or (MPosX<16)) and ((MPosY>=112) and (MPosY<128)) then end --boss
  end
 end
end

function ShowDisabledUpgrades()
 if (h_hyper and not hyper_on) then
  DrawTileP(0x0356B0,14*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0356C0,14*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0357B0,15*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0357C0,15*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0358B0,16*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0358C0,16*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0359B0,17*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0359C0,17*8,5*8,PT6,PT7,PT4,PT4)
  end
 if (h_crusher and not crusher_on) then
  DrawTileP(0x035690,4*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0356A0,4*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035790,5*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0357A0,5*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035890,6*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0358A0,6*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035990,7*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x0359A0,7*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035A90,8*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x035AA0,8*8,5*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035B90,9*8,4*8,PT6,PT7,PT4,PT4) DrawTileP(0x035BA0,9*8,5*8,PT6,PT7,PT4,PT4)
  end
 if (hover_bar and not h_hover) then
  DrawTileP(0x035290,24*8,8*8,PT6,PT7,PT4,PT4) DrawTileP(0x0352A0,24*8,9*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035390,25*8,8*8,PT6,PT7,PT4,PT4) DrawTileP(0x0353A0,25*8,9*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035490,26*8,8*8,PT6,PT7,PT4,PT4) DrawTileP(0x0354A0,26*8,9*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x035590,27*8,8*8,PT6,PT7,PT4,PT4) DrawTileP(0x0355A0,27*8,9*8,PT6,PT7,PT4,PT4)
   elseif (hover_bar) then
  DrawTileP(0x035290,24*8,8*8,PT5,PT6,PT7,PT4) DrawTileP(0x0352A0,24*8,9*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x035390,25*8,8*8,PT5,PT6,PT7,PT4) DrawTileP(0x0353A0,25*8,9*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x035490,26*8,8*8,PT5,PT6,PT7,PT4) DrawTileP(0x0354A0,26*8,9*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x035590,27*8,8*8,PT5,PT6,PT7,PT4) DrawTileP(0x0355A0,27*8,9*8,PT5,PT6,PT7,PT4)
  end
 if (ab_dive and not h_dive) then
  DrawTileP(0x0352B0,24*8,10*8,PT6,PT7,PT4,PT4) DrawTileP(0x0352C0,24*8,11*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0353B0,25*8,10*8,PT6,PT7,PT4,PT4) DrawTileP(0x0353C0,25*8,11*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0354B0,26*8,10*8,PT6,PT7,PT4,PT4) DrawTileP(0x0354C0,26*8,11*8,PT6,PT7,PT4,PT4)
   elseif (ab_dive) then
  DrawTileP(0x0352B0,24*8,10*8,PT5,PT6,PT7,PT4) DrawTileP(0x0352C0,24*8,11*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0353B0,25*8,10*8,PT5,PT6,PT7,PT4) DrawTileP(0x0353C0,25*8,11*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0354B0,26*8,10*8,PT5,PT6,PT7,PT4) DrawTileP(0x0354C0,26*8,11*8,PT5,PT6,PT7,PT4)
  end
 if (ab_wall1 and not h_wall1) then
  DrawTileP(0x0352D0,24*8,12*8,PT6,PT7,PT4,PT4) DrawTileP(0x0352E0,24*8,13*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0353D0,25*8,12*8,PT6,PT7,PT4,PT4) DrawTileP(0x0353E0,25*8,13*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0354D0,26*8,12*8,PT6,PT7,PT4,PT4) DrawTileP(0x0354E0,26*8,13*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0355B0,27*8,12*8,PT6,PT7,PT4,PT4) DrawTileP(0x0355C0,27*8,13*8,PT6,PT7,PT4,PT4)
   elseif (ab_wall1) then
  DrawTileP(0x0352D0,24*8,12*8,PT5,PT6,PT7,PT4) DrawTileP(0x0352E0,24*8,13*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0353D0,25*8,12*8,PT5,PT6,PT7,PT4) DrawTileP(0x0353E0,25*8,13*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0354D0,26*8,12*8,PT5,PT6,PT7,PT4) DrawTileP(0x0354E0,26*8,13*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0355B0,27*8,12*8,PT5,PT6,PT7,PT4) DrawTileP(0x0355C0,27*8,13*8,PT5,PT6,PT7,PT4)
  end
 if (ab_wall2 and not h_wall2) then
  DrawTileP(0x0352D0,24*8,14*8,PT6,PT7,PT4,PT4) DrawTileP(0x0352E0,24*8,15*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0353D0,25*8,14*8,PT6,PT7,PT4,PT4) DrawTileP(0x0353E0,25*8,15*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0354D0,26*8,14*8,PT6,PT7,PT4,PT4) DrawTileP(0x0354E0,26*8,15*8,PT6,PT7,PT4,PT4)
  DrawTileP(0x0355D0,27*8,14*8,PT6,PT7,PT4,PT4) DrawTileP(0x0355E0,27*8,15*8,PT6,PT7,PT4,PT4)
   elseif (ab_wall2) then
  DrawTileP(0x0352D0,24*8,14*8,PT5,PT6,PT7,PT4) DrawTileP(0x0352E0,24*8,15*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0353D0,25*8,14*8,PT5,PT6,PT7,PT4) DrawTileP(0x0353E0,25*8,15*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0354D0,26*8,14*8,PT5,PT6,PT7,PT4) DrawTileP(0x0354E0,26*8,15*8,PT5,PT6,PT7,PT4)
  DrawTileP(0x0355D0,27*8,14*8,PT5,PT6,PT7,PT4) DrawTileP(0x0355E0,27*8,15*8,PT5,PT6,PT7,PT4)
  end
 -- additionally, force hyper and wall1 on for crusher and wall2 (maybe make sure they're collected?)
 if (crusher_on and not hyper_on) then memory.writebyte(0x03FC,abilities+1) end
 if (h_wall2 and not h_wall1) then memory.writebyte(0x0099,powerups+4) end
end
function EquipControls() 
if (U_F3 == 4) then --down
 EquipCursor = EquipCursor +1
 PlaySFX(0x19)
 end
if (U_F3 == 8) then --up
 EquipCursor = EquipCursor -1
 PlaySFX(0x19)
 end
if (EquipCursor < 0) then EquipCursor = 6 end --order will be 6=Wall2 -> 1=Hyper, skipping the key
if (EquipCursor > 6) then EquipCursor = 0 end
--
if (EquipCursor == 1) then
 if (h_hyper) then
  if (U_10%4 < 2) then gui.rect(14*8,4*8,18*8-1,6*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (hyper_on) then memory.writebyte(0x03FC,abilities-1)
	else memory.writebyte(0x03FC,abilities+1) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then EquipCursor = 0 end
 end
end
if (EquipCursor == 2) then
 if (h_crusher) then
  if (U_10%4 < 2) then gui.rect(4*8,4*8,10*8-1,6*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (crusher_on) then memory.writebyte(0x03FC,abilities-2)
	else memory.writebyte(0x03FC,abilities+2) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then EquipCursor = 0+(h_hyper and 1 or 0) end
 end
end
if (EquipCursor == 3) then
 if (hover_bar) then
  if (U_10%4 < 2) then gui.rect(24*8,8*8,28*8-1,10*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (h_hover) then memory.writebyte(0x0099,powerups-1)
	else memory.writebyte(0x0099,powerups+1) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then
   if (h_crusher) then EquipCursor = 2
   elseif (h_hyper) then EquipCursor = 1
   else EquipCursor = 0 end
  end
 end
end
if (EquipCursor == 4) then
 if (ab_dive) then
  if (U_10%4 < 2) then gui.rect(24*8,10*8,28*8-1,12*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (h_dive) then memory.writebyte(0x0099,powerups-2)
	else memory.writebyte(0x0099,powerups+2) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then 
   if (hover_bar) then EquipCursor = 3
   elseif (h_crusher) then EquipCursor = 2
   elseif (h_hyper) then EquipCursor = 1
   else EquipCursor = 0 end
  end
 end
end
if (EquipCursor == 5) then
 if (ab_wall1) then
  if (U_10%4 < 2) then gui.rect(24*8,12*8,28*8-1,14*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (h_wall1) then memory.writebyte(0x0099,powerups-4)
	else memory.writebyte(0x0099,powerups+4) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then 
   if (ab_dive) then EquipCursor = 4
   elseif (hover_bar) then EquipCursor = 3
   elseif (h_crusher) then EquipCursor = 2
   elseif (h_hyper) then EquipCursor = 1
   else EquipCursor = 0 end
  end
 end
end
if (EquipCursor == 6) then
 if (ab_wall2) then
  if (U_10%4 < 2) then gui.rect(24*8,14*8,28*8-1,16*8-1,PS2,PS2) end
  if (U_F3 == 0x80) then
   PlaySFX(0x41)
    if (h_wall2) then memory.writebyte(0x0099,powerups-8)
	else memory.writebyte(0x0099,powerups+8) end
  end
 else
  if (U_F3 == 4) then EquipCursor = EquipCursor +1 end
  if (U_F3 == 8) then 
   if (ab_wall1) then EquipCursor = 5
   elseif (ab_dive) then EquipCursor = 4
   elseif (hover_bar) then EquipCursor = 3
   elseif (h_crusher) then EquipCursor = 2
   elseif (h_hyper) then EquipCursor = 1
   else EquipCursor = 0 end
  end
 end
end

end

-- Region: 0 = JPN, 1 = USA, 2 = PAL
if (rom.readbyte(0x00001E) == 0x4E) then
Region = 0
elseif (rom.readbyte(0x004007) == 0x00) then
Region = 1
elseif (rom.readbyte(0x004007) == 0x4c) then
Region = 2
-- remember to adjust the status bars.
else
error("Unrecognized ROM version. Defaulting to Blaster Master (U).")
Region = 1
end
if (Region == 0) then
vPause = 0xC4
vIngame = 0xC3
vScene = 0xC2
offbars = 0
elseif (Region == 1) then
vPause = 0xC5
vIngame = 0xC3
vScene = 0xC2
offbars = 0
elseif (Region == 2) then
vPause = 0xC5
vIngame = 0xC4
vScene = 0xC3
offbars = -32
end

while (true) do
rampals(0x0858)
U_F3 = memory.readbyte(0x00f3) --controller press, make a function for this
U_F7 = memory.readbyte(0x00f7) --controller hold
havepowerups()
CurrArea = memory.readbyte(0x0014)
InTank = (memory.readbyte(0x00c1) > 0)
CurrHP = memory.readbyte(0x040d)
SofiaIdleHP = memory.readbyte(0x03ff)
EqipSub = memory.readbyte(0x00ba)
HoverFuel = memory.readbyte(0x0092)
GunLV = memory.readbyte(0x00c3)
Lives = memory.readbyte(0x00dd)
Continues = memory.readbyte(0x037e)
HMissiles = memory.readbyte(0x06f0) 
ThndrBrks = memory.readbyte(0x06f1)
TriMissls = memory.readbyte(0x06f2)
PlayerMX = memory.readbyte(0x0403)
PlayerMY = memory.readbyte(0x0405)
SofiaMX = memory.readbyte(0x03d1)
SofiaMY = memory.readbyte(0x03d3)
SofiaMap = memory.readbyte(0x03d4)

DefeatedBosses = memory.readbyte(0x03fb)

ProgramMode = memory.readbyte(0x01ff)
U_10 = memory.readbyte(0x0010) --frame counter?
--ProgramSubMode
U_652 = memory.readbyte(0x0652)
--U_6B = memory.readbyte(0x006b) --in reality just PS3
--aaagg = ((U_6B == 2) or (memory.readbyte(0x0069)==0x10) or (memory.readbyte(0x0069)==0) or (memory.readbyte(0x006a) == 5))
U_663 = memory.readbyte(0x0663) --real palette PS3
LivesScreen = ((U_663 == 2) and (U_652 == 0x0f))
-- 0x0400 = player sprite
-- 0x01ff c2 title, c3 game, c5 pause/area change
-- 0x0652 15 pause/intro/BOSS, 00 main game, 17 grotto/intro, 0f lives/cont/BOSS., 2c title, 07 start scene
-- 0x065f 07 pause, 10 game
-- 0x006b 0f ingame 02 lives?
--remaininglivesdisplay = (((memory.readbyte(0x0400) == 4) or (memory.readbyte(0x0400) == 0x1c)) and (memory.readbyte(0x0652) == 0x0f) and (PS3 == "P02")) 
IsInGrotto = (CurrArea < 8)
if (IsInGrotto) then CurrAreaN = CurrArea else CurrAreaN = CurrArea - 8 end

BadPauseScreen = ((memory.readbyte(0x0655) == 0x30) and (memory.readbyte(0x0656) == 00) and (memory.readbyte(0x0657) == 0x0B))
--bad pause screen check based on palettes

--0x81 beam1 damage
--0x040f beam1 direction 

if ((ProgramMode == vIngame) and (not LivesScreen)) then -- find a way to detect lives display w/ conflict
--gradual HP bar
gui.rect(16,161+offbars,23,193+offbars,PS3,"clear")
gui.rect(16,193-(CurrHP/8)+offbars,23,193+offbars,PS1,"clear")
gui.line(18,165+offbars,21,165+offbars,PS2) gui.line(18,169+offbars,21,169+offbars,PS2) gui.line(18,173+offbars,21,173+offbars,PS2) gui.line(18,177+offbars,21,177+offbars,PS2)
gui.line(18,181+offbars,21,181+offbars,PS2) gui.line(18,185+offbars,21,185+offbars,PS2) gui.line(18,189+offbars,21,189+offbars,PS2)
--show Sofia's HP small
 gui.rect(12,177+offbars,16,193+offbars,PS3,"clear")
 if (InTank) then
 gui.rect(12,193-math.floor((CurrHP+1)/32)*2+offbars,16,193+offbars,PS1,"clear")
 else
 gui.rect(12,193-math.floor((SofiaIdleHP+1)/32)*2+offbars,16,193+offbars,PS1,"clear")
 end
 gui.line(12,177+offbars,12,192+offbars,PS2) gui.line(12,177+offbars,16,177+offbars,PS2) gui.line(12,179+offbars,16,179+offbars,PS2) gui.line(12,181+offbars,16,181+offbars,PS2)
 gui.line(12,183+offbars,16,183+offbars,PS2) gui.line(12,185+offbars,16,185+offbars,PS2) gui.line(12,187+offbars,16,187+offbars,PS2) gui.line(12,189+offbars,16,189+offbars,PS2)
 gui.line(12,191+offbars,16,191+offbars,PS2)
if (hover_bar) then --show both gun and hov bars
 if (IsInGrotto) then
  DrawTileP(0x0205D0,8,129+offbars,PS5,PS6,PS7) DrawTileP(0x0205E0,8,137+offbars,PS5,PS6,PS7)
  --main
   gui.rect(16,97+offbars,23,128+offbars,"clear",PS6) gui.rect(16,97+offbars,23,129+offbars,PS7,"clear")
   gui.rect(16,129-(GunLV/8)+offbars,23,129+offbars,PS5,"clear")
   gui.line(18,101+offbars,21,101+offbars,PS6) gui.line(18,105+offbars,21,105+offbars,PS6) gui.line(18,109+offbars,21,109+offbars,PS6) gui.line(18,113+offbars,21,113+offbars,PS6)
   gui.line(18,117+offbars,21,117+offbars,PS6) gui.line(18,121+offbars,21,121+offbars,PS6) gui.line(18,125+offbars,21,125+offbars,PS6)
  --small side
   gui.rect(12,113+offbars,16,129+offbars,PS3,"clear")
   gui.rect(12,129-math.floor((HoverFuel+1)/32)*2+offbars,16,129+offbars,PS5,"clear")
   gui.line(12,113+offbars,12,128+offbars,PS6) gui.line(12,113+offbars,16,113+offbars,PS6) gui.line(12,115+offbars,16,115+offbars,PS6) gui.line(12,117+offbars,16,117+offbars,PS6)
   gui.line(12,119+offbars,16,119+offbars,PS6) gui.line(12,121+offbars,16,121+offbars,PS6) gui.line(12,123+offbars,16,123+offbars,PS6) gui.line(12,125+offbars,16,125+offbars,PS6)
   gui.line(12,127+offbars,16,127+offbars,PS6)
 else
  DrawTileP(0x030750,8,129+offbars,PS5,PS6,PS7) DrawTileP(0x030760,8,137+offbars,PS5,PS6,PS7)
  --main
   gui.rect(16,97+offbars,23,128+offbars,"clear",PS6) gui.rect(16,97+offbars,23,129+offbars,PS7,"clear")
   gui.rect(16,129-(HoverFuel/8)+offbars,23,129+offbars,PS5,"clear")
   gui.line(18,101+offbars,21,101+offbars,PS6) gui.line(18,105+offbars,21,105+offbars,PS6) gui.line(18,109+offbars,21,109+offbars,PS6) gui.line(18,113+offbars,21,113+offbars,PS6)
   gui.line(18,117+offbars,21,117+offbars,PS6) gui.line(18,121+offbars,21,121+offbars,PS6) gui.line(18,125+offbars,21,125+offbars,PS6)
  --small side
   gui.rect(12,113+offbars,16,129+offbars,PS3,"clear")
   gui.rect(12,129-math.floor((GunLV+1)/32)*2+offbars,16,129+offbars,PS5,"clear")
   gui.line(12,113+offbars,12,128+offbars,PS6) gui.line(12,113+offbars,16,113+offbars,PS6) gui.line(12,115+offbars,16,115+offbars,PS6) gui.line(12,117+offbars,16,117+offbars,PS6)
   gui.line(12,119+offbars,16,119+offbars,PS6) gui.line(12,121+offbars,16,121+offbars,PS6) gui.line(12,123+offbars,16,123+offbars,PS6) gui.line(12,125+offbars,16,125+offbars,PS6)
   gui.line(12,127+offbars,16,127+offbars,PS6)
 end
else --show gun gauge alone if no hover
 DrawTileP(0x030750,16,129+offbars,PS5,PS6,PS7,PS4) DrawTileP(0x030760,16,137+offbars,PS5,PS6,PS7,PS4)
 gui.rect(16,97+offbars,23,128+offbars,"clear",PS6) gui.rect(16,97+offbars,23,129+offbars,PS7,"clear")
 gui.rect(16,129-(GunLV/8)+offbars,23,129+offbars,PS5,"clear")
 gui.line(18,101+offbars,21,101+offbars,PS6) gui.line(18,105+offbars,21,105+offbars,PS6) gui.line(18,109+offbars,21,109+offbars,PS6) gui.line(18,113+offbars,21,113+offbars,PS6)
 gui.line(18,117+offbars,21,117+offbars,PS6) gui.line(18,121+offbars,21,121+offbars,PS6) gui.line(18,125+offbars,21,125+offbars,PS6)
end
end

if (ProgramMode == vIngame) then --transfer gun to fuel
 if ((PlayerMX == SofiaMX) and (PlayerMY == SofiaMY) and (U_F7 == 0x44) and not(InTank)) then
  if (GunLV > 0) then 
   if (HoverFuel < 0xFF) then
    memory.writebyte(0x00c3,GunLV-1)
	memory.writebyte(0x0092,HoverFuel+1)
	elseif ((SofiaIdleHP < 0xFF)) then
	memory.writebyte(0x00c3,GunLV-1)
	if (U_10%2 == 1) then memory.writebyte(0x03ff,SofiaIdleHP+1) end
   end
  PlaySFX(0x23)
  end
  if (GunLV <= 0) then PlaySFX(0x0c) end
 end
end

if (ProgramMode == vPause) and (BadPauseScreen) then --isolate pause menu better
ShowDisabledUpgrades()
if (U_F3 == 0x20) then 
ShowMapM = (not ShowMapM)
PlaySFX(0x41) 
end
 if (ShowMapM) then
  gui.rect(5*8,4*8,27*8-1,18*8,PT8) --draws box
  DrawTube(6,4,0,21) DrawTube(5,5,1,14) DrawTube(26,5,1,14)
  DrawTileP(0x035190,5*8,4*8,PTD,PTE,PTF,PT0) DrawTileP(0x035190,26*8,4*8,PTD,PTE,PTF,PT0,false,true)
  DrawCurrMap()
 end
if (not ShowMapM) then
 EquipControls()
end
else
ShowMapM = false
EquipCursor = 0
end

emu.frameadvance()
end